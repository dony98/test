package com.example.testapplication.data.api

import com.example.testapplication.data.entities.User
import com.example.testapplication.data.entity.UserEntity
import retrofit2.Response
import retrofit2.http.*

interface AuthApiService {
    @POST("user")
    suspend fun doRegistration(
        @Body user: User
    ): Response<Unit>

    @GET("user/{userName}")
    suspend fun getUser(
        @Path("userName") userName: String,
    ): Response<UserEntity>
}
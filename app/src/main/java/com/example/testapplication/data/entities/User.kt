package com.example.testapplication.data.entities

data class User(
    val id: Int = 0,
    val username: String?,
    val firstName: String?,
    val lastName: String?,
    val email: String,
    val password: String?,
    val phone: String,
    val userStatus: Int?
)
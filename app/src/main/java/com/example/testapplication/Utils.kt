package com.example.testapplication

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

fun EditText.addTextWatcher(onText: (Editable) -> Unit): TextWatcher {
    val watcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            onText(s ?: return)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }
    addTextChangedListener(watcher)
    return watcher
}
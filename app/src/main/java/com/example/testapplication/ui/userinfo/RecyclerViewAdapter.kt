package com.example.testapplication.ui.userinfo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.testapplication.R


class RecyclerViewAdapter :
    ListAdapter<UserEntityItem, RecyclerViewAdapter.ViewHolder>(UserDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recycler,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val position = getItem(position)
        holder.bind(position)
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title = view.findViewById<TextView>(R.id.text_title)
        private val value = view.findViewById<TextView>(R.id.text_value)
        fun bind(item: UserEntityItem) {
            title.text = item.title
            value.text = item.value
        }
    }

}
package com.example.testapplication.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.testapplication.R
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(R.layout.fragment_main) {
    private val btnRegistration get() = button_registration
    private val btnUserInfo get() = button_user_info
    private lateinit var navController:NavController


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setUpView()
    }

    private fun setUpView() {
        btnRegistration.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_registrationFragment)
        }
        btnUserInfo.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_userInfoFragment)
        }
    }
}
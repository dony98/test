package com.example.testapplication.ui.registration

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testapplication.data.entities.User
import com.example.testapplication.data.entity.UserEntity
import com.example.testapplication.repository.RegistrationRepository
import kotlinx.coroutines.launch
import kotlin.math.log


class SharedViewModel(
    private val repository: RegistrationRepository
): ViewModel() {
    var loginValue = ""
    val userNameValue = MutableLiveData<String>()
    var firstNameValue = ""
    var lastNameValue = ""
    var emailValue = ""
    var passwordValue = ""
    var phoneValue = ""
    var userStatus = ""
    val isSuccessResponse = MutableLiveData<Boolean>()
    var errorMessage = MutableLiveData<String>()
    val userLiveData= MutableLiveData<UserEntity>()
    val isUserNameNull = MutableLiveData<Boolean>()

    fun doRegistration(){
        viewModelScope.launch {
            val response = repository.doRegistration(
                User(
                    username = loginValue,
                    firstName = firstNameValue,
                    lastName = lastNameValue,
                    email = emailValue,
                    password = passwordValue,
                    phone = phoneValue,
                    userStatus = userStatus.toInt()
                )
            )
            if(response.isSuccessful){
                isSuccessResponse.value = true
            }else{
                isSuccessResponse.value = false
                errorMessage.value = response.message()
            }
        }
    }

    fun getUser(){
        if(loginValue.isNullOrEmpty()) isUserNameNull.value =true
        else {
            viewModelScope.launch {
                val response = repository.getUser(userName = loginValue)
                if (response.isSuccessful) {
                    userLiveData.value = response.body()
                } else {
                    errorMessage.value = response.message()
                    loginValue = ""
                }
            }
            isUserNameNull.value = false
        }
    }

    fun setLogin(login: String){
        loginValue = login
        userNameValue.value = login
    }
    fun setFirstName(firstName: String){
        firstNameValue = firstName
    }
    fun setLastName(LastName: String){
        lastNameValue = LastName
    }
    fun setEmail(email: String){
        emailValue = email
    }
    fun setPassword(password: String){
        passwordValue = password
    }
    fun setPhone(phone: String){
        phoneValue = phone
        println(phone)
    }
    fun setStatus(string: String){
         userStatus = string
    }
}
package com.example.testapplication.ui.userinfo

import com.example.testapplication.data.entity.UserEntity

class UserEntityFactory {

    fun create(userEntity: UserEntity): List<UserEntityItem> {
        return mutableListOf(
            UserEntityItem(
                "id", userEntity.id.toString()
            ),
            UserEntityItem(
                "username", userEntity.username
            ),
            UserEntityItem(
                "firstName", userEntity.firstName
            ),
            UserEntityItem(
                "lastName", userEntity.lastName
            ),
            UserEntityItem(
                "email", userEntity.email
            ),
            UserEntityItem(
                "password", userEntity.password
            ),
            UserEntityItem(
                "phone", userEntity.phone
            ),
            UserEntityItem(
                "userStatus", userEntity.userStatus.toString()
            )
        )
    }
}
package com.example.testapplication.ui.registration

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.testapplication.R
import com.example.testapplication.addTextWatcher
import kotlinx.android.synthetic.main.fragment_registration.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class RegistrationFragment : Fragment(R.layout.fragment_registration) {
    private val editLogin get() = edit_login
    private val editFirstName get() = edit_name
    private val editLastName get() = edit_surname
    private val editEmail get() = edit_email
    private val editPassword get() = edit_password
    private val editPhone get() = edit_phone
    private val selectedView get() = edit_status
    private val btnRegistration get() = button_registration

    private val sharedViewModel by sharedViewModel<SharedViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
        setUpViewModel()
    }

    private fun setUpViewModel() {
        sharedViewModel.isSuccessResponse.observe(viewLifecycleOwner,{
            if(it) findNavController().navigateUp()
            else{
                Toast.makeText(context,sharedViewModel.errorMessage.value,Toast.LENGTH_SHORT)
            }
        })
    }

    private fun setUpView() {
        editLogin.addTextWatcher {
            sharedViewModel.setLogin(it.toString())
        }
        editFirstName.addTextWatcher {
            sharedViewModel.setFirstName(it.toString())
        }
        editLastName.addTextWatcher {
            sharedViewModel.setLastName(it.toString())
        }
        editEmail.addTextWatcher {
            sharedViewModel.setEmail(it.toString())
        }
        editPassword.addTextWatcher {
            sharedViewModel.setPassword(it.toString())
        }
        editPhone.addTextWatcher {
            sharedViewModel.setPhone(it.toString())
        }
        btnRegistration.setOnClickListener {
            if (valid()) sharedViewModel.doRegistration()
        }

        selectedView.setOnClickListener {
            findNavController().navigate(R.id.action_registrationFragment_to_bottomSheetFragment2)
        }
    }

    private fun valid(): Boolean {
        if (sharedViewModel.phoneValue.isNullOrBlank()) {
            phone_error.visibility = View.VISIBLE
            return false
        } else {
            phone_error.visibility = View.GONE
        }
        if (sharedViewModel.emailValue.isNullOrBlank()) {
            error_email.text = "Email is empty"
            error_email.visibility = View.VISIBLE
            return false
        } else {
            error_email.visibility = View.GONE
        }

        if (!containMailSign(sharedViewModel.emailValue))
            return false
        else {
            error_email.visibility = View.GONE
        }
        return true
    }

    private fun containMailSign(text: String): Boolean {
        if (text.contains("@")) {
            return true
        }
        error_email.text = "Email does not contains @ sign"
        error_email.visibility = View.VISIBLE
        return false
    }
}
package com.example.testapplication.ui.userinfo

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.View
import com.example.testapplication.R
import com.example.testapplication.ui.registration.SharedViewModel
import kotlinx.android.synthetic.main.fragment_user_info.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class UserInfoFragment : Fragment(R.layout.fragment_user_info) {
    private val sharedViewModel by sharedViewModel<SharedViewModel>()

    private val factory = UserEntityFactory()
    private val adapter = RecyclerViewAdapter()
    private val recyclerView get() = recycler_user_item
    private val buttonSubmit get() = button_submit
    private val editUserName get() = edit_user_name

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpUi()
        setUpViewModel()
    }

    private fun setUpUi() {
        sharedViewModel.getUser()
        buttonSubmit.setOnClickListener {
            sharedViewModel.setLogin(editUserName.text.toString())
            editUserName.visibility = View.GONE
            buttonSubmit.visibility = View.GONE
        }
    }


    private fun setUpViewModel() {
        sharedViewModel.userLiveData.observe(viewLifecycleOwner, {
            val userList = factory.create(it)
            adapter.submitList(userList)
            recyclerView.adapter = adapter
        })

        sharedViewModel.isUserNameNull.observe(viewLifecycleOwner, {
            if (it) {
                editUserName.visibility = View.VISIBLE
                buttonSubmit.visibility = View.VISIBLE
            } else {
                editUserName.visibility = View.GONE
                buttonSubmit.visibility = View.GONE
            }
        })

        sharedViewModel.userNameValue.observe(viewLifecycleOwner, {
            sharedViewModel.getUser()
        })
    }

}
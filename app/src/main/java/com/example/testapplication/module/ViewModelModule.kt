package com.example.testapplication.module

import com.example.testapplication.ui.registration.SharedViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        SharedViewModel(get())
    }
}
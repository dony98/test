package com.example.testapplication.module

import com.example.testapplication.BuildConfig
import com.example.testapplication.data.api.AuthApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


private const val BASE_URI = "https://petstore.swagger.io/v2/"

val networkModule = module{
    single(named("auth")) { provideAuthOkhttpClient() }
    single { provideAuthApiService(get(named("auth"))) }
}

fun provideAuthOkhttpClient(): OkHttpClient {
    val okHttpClient = OkHttpClient.Builder()
        .also { client ->
            if (BuildConfig.DEBUG) {
                val logging = HttpLoggingInterceptor()
                logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                client.addInterceptor(logging)
            }
        }.build()
    return okHttpClient
}

fun provideAuthApiService(client: OkHttpClient): AuthApiService {
    val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .baseUrl(BASE_URI)
        .build()

    return retrofit.create(AuthApiService::class.java)
}

package com.example.testapplication.module

import com.example.testapplication.repository.RegistrationRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    single { RegistrationRepository() }
}
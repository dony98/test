package com.example.testapplication.repository

import com.example.testapplication.data.entities.User
import com.example.testapplication.data.entity.UserEntity
import com.example.testapplication.module.provideAuthApiService
import com.example.testapplication.module.provideAuthOkhttpClient
import retrofit2.Response

class RegistrationRepository {

    suspend fun doRegistration(userDvo: User): Response<Unit> {
        return provideAuthApiService(provideAuthOkhttpClient()).doRegistration(
            User(
                userDvo.id,
                userDvo.username,
                userDvo.firstName,
                userDvo.lastName,
                userDvo.email,
                userDvo.password,
                userDvo.phone,
                userDvo.userStatus
            )
        )
    }

    suspend fun getUser(userName: String): Response<UserEntity> {
        return provideAuthApiService(provideAuthOkhttpClient()).getUser(
            userName
        )
    }
}